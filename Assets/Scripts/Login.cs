﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Login : MonoBehaviour
{
    private WebRequest Web;
    public InputField campoNome;
    public InputField campoSenha;

    private void Awake()
    {
        Web = new WebRequest();
    }

    public async void Autenticar()
    {
        string nome = campoNome.text;
        string senha = campoSenha.text;

        string resultado = await Web.WebAutenticar(nome, senha);

        if (Web.detectarErro(resultado) == "")
        {
            int id = Web.extrairID(resultado);
            Debug.Log(id);

            PlayerPrefs.SetInt("id", id);
            SceneManager.LoadScene("Splash");
        }
    }

    public void Cadastrar()
    {
        SceneManager.LoadScene("Cadastro");
    }

    private void Start()
    {
        if(PlayerPrefs.HasKey("id"))
        {
            SceneManager.LoadScene("Splash");
        }
    }
}
