﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Cadastro : MonoBehaviour
{
    private WebRequest Web;
    public InputField campoNome;
    public InputField campoEmail;
    public InputField campoSenha;

    private void Awake()
    {
        Web = new WebRequest();
    }

    public async void Cadastrar()
    {
        string nome = campoNome.text;
        string email = campoEmail.text;
        string senha= campoSenha.text;

        var resultado = await Web.WebCadastrar(nome, email, senha);

        if(Web.detectarErro(resultado) == "")
        {
            int id = Web.extrairID(resultado);
            Debug.Log(id);

            PlayerPrefs.SetInt("id", id);
            SceneManager.LoadScene("Splash");
        }
    }
}
