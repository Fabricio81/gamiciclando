﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class GerenciaDesafio : MonoBehaviour
{
    public static DateTime ultimaEntrada;
    private static int diasConsecutivos;
    private static int jogosConcluidos;
    private static int lixoJogados;


    public static int JogosConcluidos
    {
        get
        {
            return jogosConcluidos;
        }
        set
        {
            jogosConcluidos += value;
            //PlayerPrefs.SetInt("jogosConcluidos", jogosConcluidos);
        }
    }

    public static int LixoJogados
    {
        get
        {
            return lixoJogados;
        }
        set
        {
            lixoJogados += value;
            PlayerPrefs.SetInt("jogosConcluidos", lixoJogados);
        }
    }


    public static void SalvarDados()
    {
        PlayerPrefs.SetString("ultimaEntrada", ultimaEntrada.ToString("s"));
        PlayerPrefs.SetInt("diasConsecutivos", diasConsecutivos);
        PlayerPrefs.SetInt("jogosConcluidos", JogosConcluidos);
        PlayerPrefs.SetInt("lixoJogados", LixoJogados);
    }

    public static void CarregarDados()
    {
        DateTime dataAgora = DateTime.UtcNow;

        ultimaEntrada = DateTime.Parse(PlayerPrefs.GetString("ultimaEntrada"));
        diasConsecutivos = PlayerPrefs.GetInt("diasConsecutivos");
        JogosConcluidos = PlayerPrefs.GetInt("jogosConcluidos");
        LixoJogados = PlayerPrefs.GetInt("lixoJogados");

        int diferencaDias = (ultimaEntrada - dataAgora).Days;

        if (diferencaDias == 1)
        {
            diasConsecutivos += 1;
            PlayerPrefs.SetInt("diasConsecutivos", diasConsecutivos);
        }
        else if (diferencaDias > 1)
        {
            diasConsecutivos = 0;
            PlayerPrefs.SetInt("diasConsecutivos", diasConsecutivos);
        }

        ultimaEntrada = dataAgora;
        PlayerPrefs.SetString("ultimaEntrada", ultimaEntrada.ToString("s"));
    }

    private static bool VerificarDados()
    {
        bool entrada = PlayerPrefs.HasKey("ultimaEntrada");
        bool dias = PlayerPrefs.HasKey("diasConsecutivos");
        bool jogos = PlayerPrefs.HasKey("jogosConcluidos");
        bool lixos = PlayerPrefs.HasKey("lixoJogados");

        return entrada && dias && jogos && lixos;
    }

    private void Awake()
    {
        if(!VerificarDados())
        {
            ultimaEntrada = DateTime.UtcNow;
            diasConsecutivos = 0;
            JogosConcluidos = 0;
            LixoJogados = 0;
        }
        else
        {
            CarregarDados();
        }
    }
}
