﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeletorLixo : MonoBehaviour
{
    public Transform pontoLixo;
    
    public GameObject[] lixos;

    [HideInInspector] public GameObject lixoSelecionado;
    [HideInInspector] public int posicaoSelecionado;

    private void Start()
    {
        posicaoSelecionado = 0;
        AparecerLixo(lixos[0]);
    }

    private void PercorrerSelecao(int direcao)
    {
        posicaoSelecionado += direcao;
        posicaoSelecionado %= lixos.Length;

        if(posicaoSelecionado < 0)
        {
            posicaoSelecionado = lixos.Length - 1;
        }
    }

    public void AparecerLixo()
    {
        AparecerLixo(lixos[posicaoSelecionado]);
    }

    private void AparecerLixo(GameObject lixo)
    {
        lixoSelecionado = Instantiate(lixo, pontoLixo.position, pontoLixo.rotation) as GameObject;
    }

    public void EscolherLixo(int direcao)
    {
        Destroy(lixoSelecionado);

        PercorrerSelecao(direcao);

        AparecerLixo(lixos[posicaoSelecionado]);
    }
}
