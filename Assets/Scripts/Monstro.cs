﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public enum TipoLixo
{
	Metal,
	Plastico,
	Vidro,
	Papel,
	Organico,
}

public class Monstro : MonoBehaviour 
{
	public TipoLixo tipoLata;

	private void OnTriggerEnter2D(Collider2D other)
	{
		Lixo tipo = other.gameObject.GetComponent<Lixo>();

		if(tipo.tipoLixo == this.tipoLata)
		{
			Debug.Log("Lixo Correto");
		}

		Destroy(other.gameObject);
        Lixo.seletorLixo.AparecerLixo();
		SceneManager.LoadScene("Splash");
	}
}
