﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GerenciaZona : MonoBehaviour
{
	public Objeto.Ponto zonaAtual;
	public static GerenciaZona instancia;

	private void Awake() 
	{
		if( instancia != null && instancia != this )
		{
			Destroy( this.gameObject );
		}
		else
		{
			instancia = this;
			DontDestroyOnLoad( this.gameObject );
		}
    }
}
