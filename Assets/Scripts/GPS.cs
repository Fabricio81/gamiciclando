﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GPS : MonoBehaviour
{
	public LocationInfo posicaoJogador;
	private static GPS instancia;

	private void Awake() 
	{
		if( instancia != null && instancia != this )
		{
			Destroy( this.gameObject );
		}
		else
		{
			instancia = this;
			DontDestroyOnLoad( this.gameObject );
		}
	}

    private IEnumerator Start()
	{
		if( !Input.location.isEnabledByUser )
		{
			yield break;
		}

		Input.location.Start();

		int espera = 20;
		while( Input.location.status == LocationServiceStatus.Initializing && espera > 0 )
		{
			yield return new WaitForSeconds(1);
		}

		if( espera < 1 )
		{
			yield break;
		}

		if( Input.location.status == LocationServiceStatus.Failed )
		{
			yield break;
		}
		else
		{
			posicaoJogador = Input.location.lastData;
		}

		Input.location.Stop();
	}

}
