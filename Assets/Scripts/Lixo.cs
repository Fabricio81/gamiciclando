﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lixo : MonoBehaviour 
{
    public static SeletorLixo seletorLixo = null;
	public TipoLixo tipoLixo;

    private void Awake()
    {
        if(Lixo.seletorLixo == null)
        {
            Lixo.seletorLixo = GameObject.Find("SeletorLixo").GetComponent<SeletorLixo>();
        }
    }

    public void OnMouseUp()
    {
        transform.position = seletorLixo.pontoLixo.position;
    }

    private void OnMouseDrag()
    {
		Vector3 mouse = Input.mousePosition;
		mouse.z = 10;
		mouse = Camera.main.ScreenToWorldPoint(mouse);

		transform.position = mouse;
	}
}
