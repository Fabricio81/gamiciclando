﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Objeto
{
    [CreateAssetMenu(fileName = "Desafio", menuName = "ScriptableObjects/Desafio", order = 3)]
    public class Desafio : ScriptableObject
    {
        public int[] milestones = new int[5];
    }
}

