﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Objeto
{
	public class Usuario
	{
		public string matricula;
		public string nome;
		public string apelido;
	}

	public class Medalha
	{
		public string nome;
		public string progresso;
		public bool concluido;
	}

	public class Estatistica
	{
		public int totalMetalDescartado;
		public int totalPapelDescartado;
		public int totalVidroDescartado;
		public int totalPlasticoDescartado;
		public int totalOrganicoDescartado;
		public float tempoJogado;
		public int totalVitorias;
		public int diasConsecutivos;
		public Medalha[] medalhas;
	}

	public class Jogador
	{
		public int nivel;
		public int pontuacao;
		public int totalMoedas;

		public Usuario dadosUsuario;
		public Estatistica statsColetados;
	}
}
