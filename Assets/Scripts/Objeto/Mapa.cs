﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Objeto
{
	[CreateAssetMenu(fileName = "Pontos do Mapa", menuName = "ScriptableObjects/Mapa", order = 1)]
	public class Mapa : ScriptableObject
	{
		public Ponto[] pontosMapa;
		public float raioAlcance;
	}
}