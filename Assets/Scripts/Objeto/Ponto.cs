﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

namespace Objeto
{
	[CreateAssetMenu(fileName = "Ponto", menuName="ScriptableObjects/Ponto", order = 2)]
	public class Ponto : ScriptableObject
	{
		public double latitude;
		public double longitude;
		public Sprite imagem;

		public GameObject[] ordem;
	}
}
