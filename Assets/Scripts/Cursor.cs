﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cursor : MonoBehaviour
{
    private SeletorLixo seletorLixo;

    public void Start()
    {
        seletorLixo = GameObject.Find("SeletorLixo").GetComponent<SeletorLixo>();
    }

    private void OnMouseDown()
    {
        int direcao = transform.eulerAngles.z == 90 ? -1 : 1;
        seletorLixo.EscolherLixo(direcao);
    }
}
