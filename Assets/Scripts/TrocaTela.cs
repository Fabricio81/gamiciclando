﻿using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine;

public class TrocaTela : MonoBehaviour
{
    public IEnumerator TrocarTela()
    {
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene("Mapa");
    }

    public void Start()
    {
        StartCoroutine(TrocarTela());
    }
}
