﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.Networking;
using UnityEngine;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace WebObject
{
    [Serializable]
    public class Cliente
    {
        public string UserName;
        public string Email;
    }

    [Serializable]
    public class Usuario
    {
        public Cliente cliente;
        public string password;
    }

    [Serializable]
    public class Auth
    {
        public string userName;
        public string password;
    }

    [Serializable]
    public class Jogador
    {
        public int id;
        public int xp;
        public int tempoJogo;
        public string nickname;
        public string imagemId;
    }

    public class Retorno
    {
        public string resultado;
    }
}

public class WebRequest 
{
    private const string LINK_MAIN = "http://ec2-3-93-81-65.compute-1.amazonaws.com/api";

    private const string LINK_RANKING = LINK_MAIN + "/avatar/ranking";
    private const string LINK_CADASTRO = LINK_MAIN + "/auth/register";
    private const string LINK_AUTH = LINK_MAIN + "/auth/signin";

    public int extrairID(string resultado)
    {
        Regex RxAvatar = new Regex(@"""avatar"":{""id"":\d+", RegexOptions.IgnoreCase);
        Regex RxID = new Regex(@"\d+", RegexOptions.IgnoreCase);

        string avatar = RxAvatar.Match(resultado.ToString()).Value;
        string id = RxID.Match(avatar).Value;

        return int.Parse(id);
    }

    public string detectarErro(string retorno)
    {
        Regex RxErro = new Regex(@"ERRO: ", RegexOptions.IgnoreCase);
        if(RxErro.IsMatch(retorno))
        {
            retorno.Replace("ERRO: ", "");
            return retorno;
        }
        else
        {
            return "";
        }
    }

    private async Task<string> WebRequisitar(string link, string metodo, string conteudo)
    {
        byte[] corpo = Encoding.UTF8.GetBytes(conteudo);

        UnityWebRequest requisicao = new UnityWebRequest(link, metodo);
        requisicao.SetRequestHeader("content-type", "application/json");

        requisicao.uploadHandler = new UploadHandlerRaw(corpo);
        requisicao.downloadHandler = new DownloadHandlerBuffer();
        requisicao.chunkedTransfer = false;
        
        await requisicao.SendWebRequest();

        if (requisicao.isNetworkError || requisicao.isHttpError)
        {
            Debug.Log(requisicao.error);
            return "ERRO: " + requisicao.error;
        }
        else
        {
            byte[] resposta = requisicao.downloadHandler.data;
            string resultado = Encoding.UTF8.GetString(resposta, 0, resposta.Length);

            Debug.Log(resultado);
            return resultado;
        }
    }


    public async Task<string> WebCadastrar(string nome, string email, string senha)
    {
        var novoUsuario = new WebObject.Usuario
        {
            cliente = new WebObject.Cliente { UserName = nome, Email = email },
            password = senha
        };

        string jsonUsuario = JsonUtility.ToJson(novoUsuario);
        Debug.Log(jsonUsuario);

        return await WebRequisitar(LINK_CADASTRO, UnityWebRequest.kHttpVerbPOST, jsonUsuario);
    }


    public async Task<string> WebAutenticar(string nome, string senha)
    {
        WebObject.Retorno resultado = new WebObject.Retorno();
        var usuario = new WebObject.Auth
        {
            userName = nome,
            password = senha
        };

        string jsonUsuario = JsonUtility.ToJson(usuario);
        Debug.Log(jsonUsuario);

        return await WebRequisitar(LINK_AUTH, UnityWebRequest.kHttpVerbPOST, jsonUsuario);
        
    }


    public async Task<string> WebRanking()
    {
        UnityWebRequest requisicao = UnityWebRequest.Get(LINK_RANKING + "/1");
        requisicao.downloadHandler = new DownloadHandlerBuffer();

        await requisicao.SendWebRequest();

        if (requisicao.isNetworkError || requisicao.isHttpError)
        {
            Debug.Log(requisicao.error);
            return "ERRO: " + requisicao.error;
        }
        else
        {
            byte[] retorno = requisicao.downloadHandler.data;
            string resultado = Encoding.UTF8.GetString(retorno, 0, retorno.Length);

            Debug.Log(resultado);
            return resultado;
        }
    }
}
