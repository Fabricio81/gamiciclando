﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Coordenada : MonoBehaviour
{
	public Objeto.Ponto dadoCoordenada;

    private void OnMouseDown() 
	{
		GerenciaZona.instancia.zonaAtual = dadoCoordenada;
		SceneManager.LoadScene("Jogo");
	}
}
