﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class pontoIngame
{
	public Objeto.Ponto coordenada;
	public Transform ponto;
}

public class Mapa : MonoBehaviour
{
	public Objeto.Mapa mapa;
	public GameObject uiPonto;

	public pontoIngame origem;
	public pontoIngame limiteX;
	public pontoIngame limiteY;

	private Vector2 pontoFim;
	private Vector2 coordFim;


	private float latCalc(double x)
	{
		double resultado = (x - -10.93) * 1e6;

		return (float)resultado;
	}


	private float longCalc(double x)
	{
		double resultado = (x - -37.65) * 1e6;

		return (float)resultado;
	}


	private void Awake() {
		Debug.Log(limiteX.ponto.position + " -> (" + limiteX.coordenada.longitude + ", " + limiteX.coordenada.latitude + ")");
		Debug.Log(limiteY.ponto.position + " -> (" + limiteY.coordenada.longitude + ", " + limiteY.coordenada.latitude + ")");

		pontoFim = new Vector2(limiteX.ponto.position.x, limiteY.ponto.position.y);

		coordFim = new Vector2(longCalc(limiteX.coordenada.longitude), latCalc(limiteY.coordenada.latitude));

		Debug.Log(coordFim);
	}


    public void GerarPontos()
    {
        foreach(Objeto.Ponto ponto in mapa.pontosMapa)
		{
			uiPonto.GetComponent<Coordenada>().dadoCoordenada = ponto;
			Debug.Log(String.Format("{0} - {1}", longCalc(ponto.longitude), latCalc(ponto.latitude)));

			float posicaoX = (longCalc(ponto.longitude) * pontoFim.x) / coordFim.x;
			float posicaoY = (latCalc(ponto.latitude) * pontoFim.y) / coordFim.y;

			Vector3 posicaoPonto = new Vector3(posicaoX, posicaoY, 1);
			Instantiate(uiPonto, posicaoPonto, uiPonto.transform.rotation);

			Debug.Log(posicaoPonto);
		}
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
