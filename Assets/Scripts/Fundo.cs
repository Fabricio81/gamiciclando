﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fundo : MonoBehaviour
{
    public GameObject agrupaMonstro;

    void Start()
    {
        var zonaAtual = GerenciaZona.instancia.zonaAtual;
        var pontos = agrupaMonstro.GetComponentsInChildren<Transform>();

        
        gameObject.GetComponent<SpriteRenderer>().sprite = zonaAtual.imagem;

        for(int indice = 1; indice < pontos.Length; indice++)
        {
            var monstro = Instantiate(zonaAtual.ordem[indice - 1], pontos[indice].position, pontos[indice].rotation) as GameObject;
            monstro.transform.SetParent(agrupaMonstro.transform);
        }
    }
}
